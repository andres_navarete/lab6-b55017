package com.example.mipatrones.Personas;

public class PersonasPresenter implements PersonasInterface.Presenter {
    private PersonaModel model;
    private PersonasView view;

    public PersonasPresenter(PersonasView view) {
        this.view = view;
        this.model = new PersonaModel(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onDestroy() {

    }
}

package com.example.mipatrones;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}
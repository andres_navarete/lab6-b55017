package com.example.mipatrones.interfaces;

import android.content.Context;

import com.example.mipatrones.BaseDataItemsException;
import com.example.mipatrones.Personas.PersonaModel;

import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public interface DataBaseDataSource {
    List<PersonaModel> obtainItems (Context context) throws BaseDataItemsException;
}
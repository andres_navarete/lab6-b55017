package com.example.mipatrones.interfaces;

import android.content.Context;

import com.example.mipatrones.CantRetrieveItemsException;
import com.example.mipatrones.Personas.PersonaModel;

import java.util.List;

public interface ItemsRepository {
    List<PersonaModel> obtainItems(Context context) throws CantRetrieveItemsException;
}

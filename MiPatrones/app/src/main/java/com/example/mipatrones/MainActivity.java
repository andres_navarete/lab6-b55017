package com.example.mipatrones;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mipatrones.Database.DeploymentData;
import com.example.mipatrones.Personas.PersonDetail;
import com.example.mipatrones.Personas.PersonaModel;
import com.example.mipatrones.interfaces.MainActivityPresenter;
import com.example.mipatrones.interfaces.MainActivityView;

import java.util.List;

// Este ejemplo realiza una implementacion de una app con el patron MVP y utilizando las recomendaciones de Clean Architecture
// Se implementa con interfaces para mantener las clases desacopladas entre capas
// La app muestra al usuario una lista de elementos
// MainActivity es la actividad principal de la app he implementa la interface MainActivityView
// View (V) llama a Presenter (P) cuando se da una interaccion de usuario
// La implementacion de Presenter (P) llama a la clase de negocio GetListItemsInteractor (P) para obtener los resultados,
// en este caso la lista de elementos a mostrar
// La implementacion de GetListItemsInteractor (P) retorna los resultados y devuelve el control a la implementacion de Presenter (P)
// Interactor utiliza las clases de la capa de datos para obtener los items
// La implementacion de Presenter (P) llama a los metodos del View (V) para actualizar la UI por medio de su interface
// Las interfaces de View, Presenter, Interactor y Listener son utilizadas para lograr el desacoplamiento
// Capa de presentacion (Vista)
public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;

    private ListView list;
    private String[] names;
    private String[] images;
    private String[] descriptions;

    private List<PersonaModel> personas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mProgressBar = findViewById(R.id.progress);

        // Para el almacenamiento de la base de datos
        DeploymentData.setDatabase(getApplicationContext());

        // Llamada al Presenter
        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }



    @Override
    protected void onResume() {
        super.onResume();
        this.personas = mMainActivityPresenter.onResume(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    // Esconder el indicador de progreso de la UI
    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }

    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override
    public void setItems(List<PersonaModel> items) {
        //mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items));
        settingInfoForList(items);

        CustomListAdapter adapter = new CustomListAdapter(this, names, images, descriptions);

        list = (ListView) findViewById(R.id.list);
        list.setAdapter((ListAdapter) adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, PersonDetail.class);

                intent.putExtra("PersonModel", personas.get(position));
                startActivity(intent);
            }
        });
    }

    // Setting the variables to send at the CustomListAdapter
    private void settingInfoForList(List<PersonaModel> arr) {

        int size = arr.size();
        this.names =        new String[size];
        this.descriptions = new String[size];
        this.images =       new String[size];

        for (int position = 0; position < size; ++position) {
            this.names[position]            = arr.get(position).getName();
            this.descriptions[position]     = arr.get(position).getIdentificacion();
            this.images[position]           = arr.get(position).getIdImage();
        }
    }

    // Mostrar mensaje en la UI
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    // Evento al dar clic en la lista
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mMainActivityPresenter.onItemClicked(position);
    }
}

package com.example.mipatrones.Database;

import android.content.Context;
import android.util.Log;

import com.example.mipatrones.Personas.PersonaModel;

import java.util.ArrayList;
import java.util.List;

public class DeploymentData {
    public static void setDatabase(Context context) {
        cleanDatabase(context);
        createInitialData(context);
    }

    private static void cleanDatabase(Context context) {
        DataAccess db = new DataAccess(context);
        db.resetDatabase();
        Log.d("reset", "Database was reset");
    }

    private static void createInitialData(Context context) {
        DataAccess dataAccess = new DataAccess(context);

        List<PersonaModel> arr = new ArrayList<>();
        arr.add(new PersonaModel("1161616161", "Andres", "1"));
        arr.add(new PersonaModel("2116161616", "Daniela", "2"));
        arr.add(new PersonaModel("3577575757", "Enrique", "3"));
        arr.add(new PersonaModel("4575752785", "Cecilia", "4"));
        arr.add(new PersonaModel("5785785757", "Carmen", "5"));
        arr.add(new PersonaModel("6857578578", "Mariana", "6"));
        arr.add(new PersonaModel("7454542542", "Sofia", "7"));
        arr.add(new PersonaModel("8454524528", "Juanka", "8"));
        arr.add(new PersonaModel("9875785785", "Juan", "9"));
        arr.add(new PersonaModel("1085785785", "Pilar", "10"));
        arr.add(new PersonaModel("1184689665", "Adrian", "11"));
        arr.add(new PersonaModel("1282528428", "Jossue", "12"));

        // Saving each of the countries
        for (PersonaModel elem: arr) {
            elem.insert(context);
        }

        dataAccess.close();
        Log.d("Personas", "Initial Personas where saved in the database.");
    }
}
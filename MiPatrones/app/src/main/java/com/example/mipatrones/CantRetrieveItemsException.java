package com.example.mipatrones;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
package com.example.mipatrones.Database;


public class DatabaseContract {

    private DatabaseContract () {}

    private static final String COMMA = ", ";
    private static final String TEXT_TYPE = " TEXT ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String REAL_TYPE = " REAL ";
    private static final String PK = " PRIMARY KEY ";
    private static final String FK = " FOREIGN KEY ";
    private static final String REF = " REFERENCES ";
    private static final String NULLABLE = " null ";

    public static class PERSONAS{
        public static final String TABLE_NAME = "Personas";

        public static final String IDENTIFICACION = "Indetificacion";
        public static final String NAME = "Name";
        public static final String IDIMAGEN = "IdImagen";
        /*Create statement*/

        public static final String SQL_CREATE_PERSONAS =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        IDENTIFICACION  + TEXT_TYPE + PK    + COMMA +
                        NAME            + TEXT_TYPE + COMMA +
                        IDIMAGEN        + TEXT_TYPE + ")";

        public static final String SQL_DELETE_PERSONAS =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }

    public static String[] DeploymentScript = {
            PERSONAS.SQL_DELETE_PERSONAS,
            PERSONAS.SQL_CREATE_PERSONAS
    };

}


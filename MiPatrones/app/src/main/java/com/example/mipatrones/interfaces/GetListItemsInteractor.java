package com.example.mipatrones.interfaces;

import android.content.Context;

import com.example.mipatrones.Personas.PersonaModel;

import java.util.List;

// Capa de Negocios (Presenter o Controller)
// Interface de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter
public interface GetListItemsInteractor {
    List<PersonaModel> getItems(OnFinishedListener listener, Context context);

    interface OnFinishedListener {
        void onFinished(List<PersonaModel> items);
    }
}
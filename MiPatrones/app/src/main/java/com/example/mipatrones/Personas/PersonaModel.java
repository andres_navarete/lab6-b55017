package com.example.mipatrones.Personas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;

import com.example.mipatrones.Database.DataAccess;
import com.example.mipatrones.Database.DatabaseContract;

import java.util.ArrayList;
import java.util.List;

public class PersonaModel implements PersonasInterface.Persona {
    private String name;
    private String identificacion;
    private String idImagen;
    private PersonasPresenter presenter;

    public PersonaModel(PersonasPresenter presenter) {
        this.presenter = presenter;
    }

    public PersonaModel()  { }

    public PersonaModel(String identificacion, String name, String idImagen) {
        this.name = name;
        this.identificacion = identificacion;
        this.idImagen = idImagen;
    }

    protected PersonaModel(Parcel in) {
        name = in.readString();
        identificacion = in.readString();
        idImagen = in.readString();
    }

    public static final Creator<PersonaModel> CREATOR = new Creator<PersonaModel>() {
        @Override
        public PersonaModel createFromParcel(Parcel in) {
            return new PersonaModel(in);
        }

        @Override
        public PersonaModel[] newArray(int size) {
            return new PersonaModel[size];
        }
    };

    // ----------------- METODOS PAARA LA BASE DE DATOS -----------------------------------------
    public long insert(Context context) {
        // Values from the database
        ContentValues values = new ContentValues();

        // Setting the values
        values.put(DatabaseContract.PERSONAS.IDENTIFICACION, getIdentificacion());
        values.put(DatabaseContract.PERSONAS.NAME, getName());
        values.put(DatabaseContract.PERSONAS.IDIMAGEN, getIdImage());

        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();
        // Adding the values to the database
        long result = dataAccess.insert(DatabaseContract.PERSONAS.TABLE_NAME, values);
        dataAccess.close();
        return result;
    }

    public static List<PersonaModel> getAll(Context context) {
        // The result list of countries
        List<PersonaModel> all = new ArrayList<>();

        // Setting the access to the database
        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();

        // Getting the cursor to iterate over the countries
        Cursor cursor = dataAccess.selectAll(DatabaseContract.PERSONAS.TABLE_NAME);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            all.add(new PersonaModel(
                    cursor.getString(   cursor.getColumnIndexOrThrow(DatabaseContract.PERSONAS.IDENTIFICACION)),
                    cursor.getString(   cursor.getColumnIndexOrThrow(DatabaseContract.PERSONAS.NAME)),
                    cursor.getString(   cursor.getColumnIndexOrThrow(DatabaseContract.PERSONAS.IDIMAGEN))
            ));
            cursor.moveToNext();
        }

        cursor.close();
        dataAccess.close();
        // Return the result
        return all;
    }
    // ----------------- FIN DE METODOS PAARA LA BASE DE DATOS -------------------------------------

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getIdImage() {
        return this.idImagen;
    }

    @Override
    public void setIdImage(String idImagen) {
        this.idImagen = idImagen;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    @Override
    public String getIdentificacion() {
        return this.identificacion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(identificacion);
        dest.writeString(idImagen);
    }
}

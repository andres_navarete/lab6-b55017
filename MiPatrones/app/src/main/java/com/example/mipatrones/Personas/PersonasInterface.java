package com.example.mipatrones.Personas;

//Todas las interfaces para la manipulación de las Personas

import android.content.Context;
import android.os.Parcelable;

import java.util.List;

public interface PersonasInterface {

    // Para las instancias de Persona
    interface Persona extends Parcelable {
        // Getters and setters
        String getIdentificacion();
        void setIdentificacion(String identificacion);
        String getName();
        void setName(String name);
        String getIdImage();
        void setIdImage(String idImagen);
    }

    // Interfaz View
    interface View {
        // Método que presenta a todas las personas
        void showProgress();
        void hideProgress();
        void setItems(List<String> items);
        void showMessage(String message);
    }

    // Interfaz Presentador
    interface Presenter {
        void onResume();
        void onItemClicked(int position);
        void onDestroy();
    }

    // Interfaz Modelo
    interface Model {

        long insert(Context context); // CREATE
        List<Persona> getAll(Context context); // READ

        void setName(String name);
        String getName();
        void setIdentificacion(String identificacion);
        String getIdentificacion();
        void setIdImagen(String idImagen);
        String getIdImagen();
    }
}

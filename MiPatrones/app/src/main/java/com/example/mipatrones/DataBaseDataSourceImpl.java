package com.example.mipatrones;

import android.content.Context;

import com.example.mipatrones.Personas.PersonaModel;
import com.example.mipatrones.Personas.PersonasInterface;
import com.example.mipatrones.interfaces.DataBaseDataSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {
    @Override
    public List<PersonaModel> obtainItems(Context context) throws BaseDataItemsException {
        List<PersonaModel> items = null;
        try {
            // TODO: Obtener de la base de datos
            items = createArrayList(context);
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }

    // Esta lista debe recuperarla de la base de datos
    // para el ejemplo la inicializamos con datos dummy
    private List<PersonaModel> createArrayList(Context context) {
        // Tomando de la base de datos
        List<PersonaModel> personas = new ArrayList<>();

        PersonaModel personaModel = new PersonaModel();
        List<PersonaModel> listaPersonas = personaModel.getAll(context);

        return listaPersonas;
    }
}
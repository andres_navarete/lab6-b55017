package com.example.mipatrones;

import android.content.Context;
import android.os.Handler;

import com.example.mipatrones.Personas.PersonaModel;
import com.example.mipatrones.interfaces.GetListItemsInteractor;
import com.example.mipatrones.interfaces.ItemsRepository;

import java.util.List;

// Capa de Negocios (Presenter o Controller)
// Implementacion de GetListItemsInteractor de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter
public class GetListItemsInteractorImpl implements GetListItemsInteractor {
    private ItemsRepository mItemsRepository;

    @Override
    public List<PersonaModel> getItems(final OnFinishedListener listener, final Context context) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        List<PersonaModel> items = null;
        mItemsRepository = new ItemsRepositoryImpl();
        try {
            // obtenemos los items
            items = mItemsRepository.obtainItems(context);
        } catch (CantRetrieveItemsException e) {
            e.printStackTrace();
        }
        // Al finalizar retornamos los items
        listener.onFinished(items);
        return items;
    }
}
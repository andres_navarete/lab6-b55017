package com.example.mipatrones.Personas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.mipatrones.R;

public class PersonDetail extends AppCompatActivity {

    TextView textViewName;
    TextView textViewDescriptions;

    PersonaModel personaModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_detail);

        textViewName = (TextView)findViewById(R.id.nameTwo);
        textViewDescriptions = (TextView)findViewById(R.id.descriptionTwo);

        this.personaModel = getIntent().getParcelableExtra("PersonModel");

        textViewName.setText(personaModel.getName());
        textViewDescriptions.setText(personaModel.getIdentificacion());
    }
}
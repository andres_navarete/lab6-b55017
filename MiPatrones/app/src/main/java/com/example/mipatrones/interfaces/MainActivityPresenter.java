package com.example.mipatrones.interfaces;

import android.content.Context;

import com.example.mipatrones.Personas.PersonaModel;

import java.util.List;

public interface MainActivityPresenter {
    // resumir
    List<PersonaModel> onResume(Context context);

    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(int position);

    // destruir
    void onDestroy();
}
